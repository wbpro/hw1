function priStatusCode(code: string|number){
    if(typeof code == 'string') {
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}`)
    } else {
        console.log(`My status code is ${code} ${typeof code}`)
    }
}

priStatusCode(404)
priStatusCode("abc")